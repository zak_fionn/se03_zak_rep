import java.util.Scanner;

public class AufgabeQuersummemitWhile {

	public static void main(String[] args) {

		System.out.println("Bitte Geben sie eine Zahl ein: ");
		Scanner eingabe = new Scanner(System.in);

		int zahl = eingabe.nextInt();

		System.out.println("Quersumme = " + berechnequersumme(zahl));

	}

	public static int berechnequersumme(int zahl) {
		int ergebnis = 0;

		do {
			ergebnis = ergebnis + (zahl % 10);
			zahl = zahl / 10;
		} while (zahl != 0);

		return ergebnis;
	}

}
