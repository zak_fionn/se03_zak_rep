import java.util.Scanner;

public class AufgabeQuersumme {

	public static void main(String[] args) {
		System.out.println("Bitte Geben sie eine Zahl ein: ");
		Scanner eingabe = new Scanner(System.in);

		int zahl = eingabe.nextInt();
		
		System.out.println("Quersumme = "  + quersumme(zahl));
	}

	public static int quersumme(int zahl) {

		if (zahl <= 9) return zahl;
		
		return zahl%10 + quersumme (zahl/10);
		
	}

}
