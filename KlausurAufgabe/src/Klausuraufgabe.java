import java.util.Scanner;

public class Klausuraufgabe {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		// Eingabe
		double strStreckeKm = streckeEinlesen("Gefahrene Strecke [km]: ", tastatur);
		
		System.out.print("Benötigte Zeit [hh:mm:ss]: ");
		String strZeit = tastatur.next();

		// Verarbeitung

		String[] strStdMinSek = strZeit.split("\\:");
		int h = Integer.valueOf(strStdMinSek[0]);
		int min = Integer.valueOf(strStdMinSek[1]);
		int sec = Integer.valueOf(strStdMinSek[2]);

		double sekunden = 60.0 * 60.0 * h + 60.0 * min + sec;
		double durchschnittsgeschwindigkeitKmh = strStreckeKm / sekunden * 60.0 * 60.0;

		// Ausgabe
		System.out.printf("Sie sind im Schnitt %.2f km/h gefahren.\n", durchschnittsgeschwindigkeitKmh);

		tastatur.close();

	}

	public static double streckeEinlesen(String text, Scanner myScanner) {
		System.out.print("Gefahrene Strecke [km]: ");
		double strStreckeKm = myScanner.nextDouble();
		return strStreckeKm;
	}

}
