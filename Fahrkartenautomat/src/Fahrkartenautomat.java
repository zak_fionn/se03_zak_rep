﻿import java.util.Scanner;

class Fahrkartenautomat {

	private static double fahrkartenbestellungErfassen() {

		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag;
		int fahrkartenAnzahl;

		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();

		System.out.print("Anzahl Fahrkarten: ");
		fahrkartenAnzahl = tastatur.nextInt();

		zuZahlenderBetrag *= fahrkartenAnzahl;

		return zuZahlenderBetrag;
	}

	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.println();
			// System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag -
			// eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
	
	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}
	
	private static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void muenzeAusgeben(int betrag, String einheit) {
		System.out.printf("%s %s", betrag, einheit);
		System.out.println();
	}
	
	private static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO", rückgabebetrag);
			System.out.println();
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}
	}

	public static void main(String[] args) {

		// Achtung!!!!
		// Hier wird deutlich, warum double nicht für Geldbeträge geeignet ist.
		// =======================================
		double zuZahlenderBetrag;
		
		double rückgabebetrag;
		// Die Eingabe erfolgt anwenderfreudlich mit Dezimalpunkt: just testing
		double eingegebenerBetrag;

		// Den zu zahlenden Betrag ermittelt normalerweise der Automat
		// aufgrund der gewählten Fahrkarte(n).
		zuZahlenderBetrag = fahrkartenbestellungErfassen();

		// Geldeinwurf
		// -----------
		rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();


		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rueckgeldAusgeben(rückgabebetrag);


		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
}