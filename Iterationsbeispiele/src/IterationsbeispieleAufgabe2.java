import java.util.Scanner;

public class IterationsbeispieleAufgabe2 {
	public static void main(String[] args) {
		System.out.println("Geben sie n ein: ");
		Scanner eingabe = new Scanner(System.in);
		int n = eingabe.nextInt();
		if (n <= 20) {
			int ergebnis = berechnefakultaet(n);
			System.out.println("Die Fakult�t ist: " + ergebnis);
		} else {
			System.out.println("Sind nur Zahlen <= 20 erlaubt!");
		}

		eingabe.close();
	}

	public static int berechnefakultaet(int n) {
		if (n == 0) {
			return 1;
		}
		int ergebnis = 1;
		for (int i = 1; i <= n; i++) {

			ergebnis = ergebnis * i;
		}

		return ergebnis;
	}

}
