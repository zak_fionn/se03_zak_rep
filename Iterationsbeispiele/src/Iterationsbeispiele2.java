import java.util.Scanner;

public class Iterationsbeispiele2 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		printNNumbers("Geben Sie eine Zahl ein:", tastatur);
		printNNumbers("\nplease give a number:", tastatur);
				
	}
	
	public static void printNNumbers(String text, Scanner myScanner) {
		System.out.println(text);
		int n = myScanner.nextInt();
		
		int zaehler = 1;
		while (zaehler <= n ){
			if (zaehler == n) {
				System.out.print(zaehler);
			}
			else {			
				System.out.print(zaehler+",");
			}
				
			zaehler++;
		}
	}
	

}