import java.util.Scanner;

public class Iterationsbeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein:");
		int n = myScanner.nextInt();
		
		int zaehler = 1;
		while (zaehler <= n ){
			if (zaehler == n) {
				System.out.print(zaehler);
			}
			else {			
				System.out.print(zaehler+",");
			}
				
			zaehler++;
		}
		
	}

}